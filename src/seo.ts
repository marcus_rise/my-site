export const title = "Ilya Konstantinov Samara Web Developer";
export const description = "Ilya Konstantinov Samara Web Developer";
export const keywords: string = new Array<string>("ReactJs", "VueJs", "NodeJs", "Web", "Developer", "Samara").join(
  ", ",
);
